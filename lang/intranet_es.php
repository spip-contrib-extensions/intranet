<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/intranet?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_intranet' => 'Intranet/Extranet',

	// E
	'explications_message' => 'Mensaje personalizado para mostrar en la parte superior del formulario de identificación.',
	'explications_pages_intranet' => 'Añadir algunas páginas SPIP específicas accesibles sin estar concetados (separar con comas ",").',

	// I
	'info_intranet' => 'Identificación obligatoria',
	'info_intranet_texte' => 'Este sitio sólo es accesible para las personas identificadas.',

	// L
	'label_message' => 'Personalización del mensaje',
	'label_pages_intranet' => 'Páginas accesibles'
);
