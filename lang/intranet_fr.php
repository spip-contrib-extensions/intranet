<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/intranet.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_intranet' => 'Intranet/Extranet',

	// E
	'explications_hosts' => 'Noms d’hôtes, séparés par des virgules, autorisés à se connecter.',
	'explications_message' => 'Message personnalisé à afficher au dessus du formulaire d’identification.',
	'explications_pages_intranet' => 'Ajouter certaines pages SPIP spécifiques accessibles sans être connectés (à séparer par des virgules ",").',
	'explications_plageip' => 'IP ou plage d’IP permettant de consulter l’ensemble du site sans être connecté. Exemple : <code>10.5.0.1-10.5.22.13,10.6.134.132</code> ',

	// I
	'info_intranet' => 'Identification obligatoire',
	'info_intranet_texte' => 'Ce site n’est accessible qu’aux personnes identifiées.',

	// L
	'label_hosts' => 'Hôtes autorisés',
	'label_intranet_ouverts' => 'Permettre de sortir les objets éditoriaux un à un de l’intranet',
	'label_message' => 'Personnalisation du message',
	'label_pages_intranet' => 'Pages accessibles',
	'label_plageip' => 'Plage IP',

	// M
	'message_intranet_remettre' => 'Remettre dans l’intranet',
	'message_intranet_sortir' => 'Sortir de l’intranet'
);
