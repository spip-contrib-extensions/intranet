<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/intranet?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_intranet' => 'Intranet/Extranet',

	// E
	'explications_hosts' => 'Nomes de servidores, separados por vírgulas, autorizados a se conectar.',
	'explications_message' => 'Mensagem personalizada para exibir no formulário de identificação.',
	'explications_pages_intranet' => 'Incluir páginas do SPIP específicas acessíveis sem se estar conectado (separadas por vírgulas ",").',
	'explications_plageip' => 'IP ou gama de IPs autorizados a consultar o site sem que se esteja conectado. Exemplo: <code>10.5.0.1-10.5.22.13,10.6.134.132</code> ',

	// I
	'info_intranet' => 'Identificação obrigatória',
	'info_intranet_texte' => 'Este site é acessível apenas a pessoas identificadas.',

	// L
	'label_hosts' => 'Servidores autorizados',
	'label_intranet_ouverts' => 'Permite liberar os objetos editoriais um a um da intranet.',
	'label_message' => 'Personalização da mensagem',
	'label_pages_intranet' => 'Paginas acessíveis',
	'label_plageip' => 'Gama IP',

	// M
	'message_intranet_remettre' => 'Restringir o acesso',
	'message_intranet_sortir' => 'Liberar o acesso'
);
