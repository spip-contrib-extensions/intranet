<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-intranet?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'intranet_description' => 'Autorizar apenas os usuários logados a visitar o site.',
	'intranet_nom' => 'Intranet/Extranet',
	'intranet_slogan' => 'Autorizar apenas os usuários logados (ou dentro de uma rede interna) a visitar o site.'
);
